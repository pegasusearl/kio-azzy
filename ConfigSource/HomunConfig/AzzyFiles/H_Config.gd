extends Node


const CONFIG_HEADER := """
---------------------------
--  WARNING FOR EDITING  --
---------------------------
-- This file has been    --
-- touched by Kiola's    --
-- GUI config which has  --
-- it's own config.      --
-- Any changes made here --
-- will be ignored and   --
-- overwritten.          --
---------------------------
"""


var settings := {
	"KioHomunBase" : 0, # 1 lif, 2 ami, 3 filir
	"KioHomunSuper" : 0, # eira, bayeri, sera, dieter, eleanor
	
	"AggroHP": "0",
	"AggroSP": "0",
	"AmiBulwarkLevel": "5",
	"AoEFixedLevel": "0",
	"AoEMaximizeTargets": "0",
	"AoEReserveSP": "1",
	"AssumeHomun": "1",
	"AttackLastFullSP": "0",
	"AttackSkillReserveSP": "0",
	"AttackTimeLimit": "60000",
	"AutoComboMode": "1",
	"AutoComboSkill": "0",
	"AutoComboSpheres": "10",
	"AutoMobCount": "2",
	"AutoMobMode": "2",
	"AutoPushbackThreshold": "2",
	"AutoSkillDelay": "100",
	"AutoSkillLimit": "100",
	"BayeriHailegeStarLevel": "5",
	"BayeriStahlHornLevel": "5",
	"BayeriSteinWandLevel": "5",
	"Berserk_ComboAlways": "0",
	"Berserk_Dance": "0",
	"Berserk_IgnoreMinSP": "0",
	"Berserk_SkillAlways": "0",
	"CastTimeRatio": ".80",
	"CastleDefendThreshold": "4",
	"ChaseSPPause": "0",
	"ChaseSPPauseSP ": "-60",
	"ChaseSPPauseTime": "3000",
	"DanceMinSP": "0",
	"DefendStandby": "0",
	"DefensiveBuffOwnerMobbed": "0",
	"DieterLavaSlideLevel": "5",
	"DoNotAttackMoving": "0",
	"DoNotChase": "0",
	"DoNotUseRest": "1",
	"EiraEraseCutterLevel": "4",
	"EiraSilentBreezeLevel": "5",
	"EiraXenoSlasherLevel": "0",
	"EleanorDoNotSwitchMode": "0",
	"EleanorMidnightLevel": "5",
	"EleanorSilverveinLevel": "5",
	"EleanorSonicClawLevel": "5",
	"FilirAccelLevel": "1",
	"FilirFlitLevel": "1",
	"FleeHP": "0",
	"FollowStayBack": "0",
	"ForceKite": "0",
	"HealOwnerBreeze": "1",
	"HealOwnerHP": "99",
	"HealSelfHP": "50",
	"IdleWalkDistance": "14",
	"IdleWalkSP": "0",
	"KiteBounds": "10",
	"KiteMonsters": "0",
	"KiteParanoid": "0",
	"KiteParanoidStep": "2",
	"KiteParanoidThreshold": "2",
	"KiteStep": "5",
	"KiteThreshold": "3",
	"LagReduction": "0",
	"LastSavedDate": "\"9/13/2017 4:17:37 PM\"",
	"LavaSlideMode": "0",
	"LifEscapeLevel": "5",
	"LiveMobID": "0",
	"MirAIFriending": "0",
	"MobileAggroDist": "15",
	"MobileMoveBounds": "15",
	"MoveSticky": "1",
	"MoveStickyFight": "1",
	"OldHomunType": "2",
	"OpportunisticTargeting": "0",
	"PVPmode": "1",
	"PainkillerFriends": "0",
	"PainkillerFriendsSave": "1",
	"PoisonMistMode": "1",
	"ProvokeOwnerMobbed": "3",
	"RelativeRoute": "1",
	"RescueOwnerLowHP": "0",
	"RestXOff": "0",
	"RestYOff": "-1",
	"SeraCallLegionLevel": "5",
	"SeraParalyzeLevel": "5",
	"SeraPoisonMistLevel": "5",
	"SpawnDelay": "100",
	"StandbyFriending": "0",
	"StationaryAggroDist": "15",
	"StationaryMoveBounds": "15",
	"SteinWandTelePause": "0",
	"StickyStandby": "1",
	"StienWandTelePause": "3000",
	"SuperPassive": "0",
	"TankMonsterLimit": "4",
	"UseAttackSkill": "1",
	"UseAutoHeal": "0",
	"UseAutoPushback": "0",
	"UseAvoid": "0",
	"UseBayeriAngriffModus": "0",
	"UseBayeriGoldenPherze": "0",
	"UseBayeriHailegeStar": "1",
	"UseBayeriStahlHorn": "1",
	"UseBayeriSteinWand": "0",
	"UseBerserkAttack": "0",
	"UseBerserkMobbed": "0",
	"UseBerserkSkill": "0",
	"UseCastleDefend": "0",
	"UseCastleRoute": "0",
	"UseDanceAttack": "1",
	"UseDefensiveBuff": "0",
	"UseDieterGraniticArmor": "0",
	"UseDieterLavaSlide": "1",
	"UseDieterMagmaFlow": "0",
	"UseDieterPyroclastic": "0",
	"UseDieterVolcanicAsh": "0",
	"UseEiraEraseCutter": "1",
	"UseEiraOveredBoost": "0",
	"UseEiraSilentBreeze": "0",
	"UseEiraXenoSlasher": "0",
	"UseEleanorSonicClaw": "1",
	"UseHomunSSkillAttack": "1",
	"UseHomunSSkillChase": "1",
	"UseIdleWalk": "0",
	"UseOffensiveBuff": "0",
	"UseProvokeOwner": "0",
	"UseSeraCallLegion": "1",
	"UseSeraPainkiller": "1",
	"UseSeraParalyze": "0",
	"UseSeraPoisonMist": "0",
	"UseSkillOnly": "-1",
	"UseSmartBulwark": "0",
	"UseSteinWandOwnerMob": "2",
	"UseSteinWandSelfMob": "2",
	"UseSteinWandTele": "0",
}


var config_input_list := {}
# key_config : [@node1,@node2] # update ui when value changed

var nodes_to_notify_on_change := {} # for show hide with req
# key_config : [@node1,@node2]

"""
NOTE DURING APPLYING
make sure KiteBounds is always less than MoveBounds
apply old homun type
"""

func has_data(key:String) -> bool:
	return settings.has(key)


func get_data(key:String) -> String:
	return settings[key]


func set_data(key:String,value:String) -> void:
	settings[key] = value
	if config_input_list.has(key):
		for node in config_input_list[key]:
			node.set_value(value)
	if nodes_to_notify_on_change.has(key):
		for node in nodes_to_notify_on_change[key]:
			node.config_active_requirement_updated(key,value)


func is_data_true(key:String) -> bool:
	return int(settings[key]) > 0


func node_listen(node):
	if not config_input_list.has(node.name):
		config_input_list[node.name] = []
	config_input_list[node.name].append(node)
	
	for key in node.config_active_requirement_list:
		if not nodes_to_notify_on_change.has(key):
			nodes_to_notify_on_change[key] = []
		nodes_to_notify_on_change[key].append(node)


func apply_data():
	# generating config texts
	settings["LastSavedDate"] = "Kiola GUI " + owner.VERSION
	
	var h_config_text := CONFIG_HEADER + "\n\n"
	for key in settings:
		h_config_text += key+"="+settings[key] + "\n"


func save_config():
	
	#below are outdated applying function
	settings["LastSavedDate"] = str( OS.get_unix_time() )

	var saved_data := ""
	for key in settings:
		saved_data += key+"="+settings[key]
		saved_data += "\n"
	
	#OS.clipboard = saved_data

