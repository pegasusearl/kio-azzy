extends Node

var default_config := "paste H_Config here"

func _ready():
	return
	var settings = {}
	var config_list = default_config.split("\n",false)
	for config_item in config_list:
		var splitted_data = config_item.split("=")
		for i in splitted_data.size():
			splitted_data[i] = splitted_data[i].rstrip(" ").lstrip(" ").rstrip("\t")
		settings[splitted_data[0]] = splitted_data[1]
	#OS.clipboard = to_json(settings)
