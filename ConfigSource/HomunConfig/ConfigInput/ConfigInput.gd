extends PanelContainer
class_name ConfigInput

const MAXIMUM_TOOLTIP_WIDTH = 40
export var is_advanced := false
export var tooltip:PoolStringArray = ["i forgot to write tips here, report to me"]
#export var key_config := "AggroHP" now use node name
export var string_config := "i forgot to name this, report"

export var show_only_when_these_active := ""
var config_active_requirement_list = []


func set_value(new_value): # placeholder
	pass


func config_active_requirement_updated(config_key:String,value:String):
	refresh_visibility()


func _ready():
	config_active_requirement_list = show_only_when_these_active.split(" ",false)
	
	add_to_group("ConfigInput")
	if is_advanced:
		add_to_group("advanced")
	
	if owner.configuration_item_node.has(name):
		owner.configuration_item_node[name].append(self)
	else:
		owner.configuration_item_node[name] = [self]
	
	for sentence in tooltip:
		var word_count = MAXIMUM_TOOLTIP_WIDTH
		var words = sentence.split(" ")
		for word in words:
			if word.length() > word_count:
				word_count = MAXIMUM_TOOLTIP_WIDTH
				hint_tooltip += "\n"
				hint_tooltip += word
			else:
				word_count -= word.length()
				hint_tooltip += word
			hint_tooltip += " "
		hint_tooltip += "\n\n"
	
	hint_tooltip += "["+name+"]"


var advanced_mode_active:= false setget set_advanced
func set_advanced(new_value:bool):
	advanced_mode_active = new_value


func refresh_visibility():
	var hide_count = 0
	if is_advanced and not advanced_mode_active:
		hide_count += 1
	
	for key in config_active_requirement_list:
		if not owner.homun_config.is_data_true(key):
			hide_count += 1
	
	if hide_count > 0:
		hide()
	else:
		show()
