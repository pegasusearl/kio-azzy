extends ConfigInput


export var icon:Texture


onready var check_button = $CheckButton

func set_value(new_value:String):
	$CheckButton.pressed = bool(int(new_value))


func _ready():
	check_button.icon = icon
	check_button.hint_tooltip = hint_tooltip
	check_button.text = string_config


func _on_CheckButton_toggled(button_pressed):
	#print("toggled")
	pass
	# do not use toggled because it will also trigger
	# when you are setting value


func _on_CheckButton_pressed():
	#print("check button pressed: ",check_button.pressed,":",str(int(check_button.pressed)))
	pass
	owner.homun_config.set_data(name,str(int(check_button.pressed)))
