extends ConfigInput


export var start_index := 1
export var option_list :PoolStringArray = ["first","second"]


func set_value(new_value:String):
	$HBoxContainer/OptionButton.select(int(new_value)-start_index)


func _ready():
	for child in $HBoxContainer.get_children():
		child.hint_tooltip = hint_tooltip
	$HBoxContainer/Label.text = string_config
	
	for option in option_list:
		$HBoxContainer/OptionButton.add_item(option)


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
