extends ConfigInput
# WARING ! DO NOT USE
# if you scroll on top of it, it will modify the value while also
# scrolling the page. BAD

export var min_value := 0
export var max_value := 100


func set_value(new_value:String): #called by Main/Core
	$HBoxContainer/value.value = int(new_value)


func _ready():
	for child in $HBoxContainer.get_children():
		child.hint_tooltip = hint_tooltip
	$HBoxContainer/title.text = string_config
	$HBoxContainer/value.min_value = min_value
	$HBoxContainer/value.max_value = max_value
	$HBoxContainer/value.tick_count = max_value-min_value+1


func _on_value_value_changed(value):
	$HBoxContainer/val.text = str(value)
