extends CheckButton


func _on_advanced_toggled(button_pressed):
	toggle_mode(button_pressed)


func toggle_mode(is_advanced):
	owner.advanced_mode = is_advanced
