extends Node

onready var homun_config = $core/H_Config

const VERSION := "A0"

var configuration_item_node := {}
var advanced_mode := false setget set_advanced_mode
var advanced_mode_config_input = []
var advanced_mode_nodes = []

onready var advanced_button = $layout/settings/advanced


func _ready():
	OS.set_window_title("Bio5 Sera - Ami Eira - AzzyAI")
	for key in configuration_item_node:
		for node in configuration_item_node[key]:
			if homun_config.has_data(key):
				node.set_value(homun_config.get_data(key))
	
	for node in get_tree().get_nodes_in_group("ConfigInput"):
		homun_config.node_listen(node)
		node.refresh_visibility()
	
	for node in get_tree().get_nodes_in_group("advanced"):
		if node.is_in_group("ConfigInput"):
			advanced_mode_config_input.append(node)
		else:
			advanced_mode_nodes.append(node)
	
	advanced_button.toggle_mode(false)


func set_advanced_mode(new_value:bool):
	advanced_mode = new_value
	for node in advanced_mode_config_input:
		node.advanced_mode_active = advanced_mode
		node.refresh_visibility()
	for node in advanced_mode_nodes:
		node.visible = advanced_mode
