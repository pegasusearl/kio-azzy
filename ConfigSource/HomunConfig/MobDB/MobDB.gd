extends Node


export var id_to_name := {}
export var name_to_id := {}


export var button:bool = false setget set_button


func _ready():
	set_button("a")


func set_button(ayy):
	var file = File.new()
	file.open("res://MobDB/mob_db.yml",file.READ)
	var text = file.get_as_text()
	file.close()
	
	var current_id := 0
	var current_name := ""
	
	var lines = text.split("\n")
	print(lines.size())
	
	for line in lines:
		var data = line.split(":")
		if data.size() <= 1:
			continue
		
		data[0] = data[0].rstrip(" ").lstrip("-# ")
		data[1] = data[1].rstrip(" ").lstrip(" ")
	
		if data[0].to_lower() == "id":
			#print("izid",data[1])
			current_id = int(data[1])
		elif data[0].to_lower() == "name":
			#print("izname",data[1])
			current_name = data[1]
			id_to_name[current_id] = current_name
			name_to_id[current_name] = current_id
	
	file.open("res://MobDB/mob_db.json",file.WRITE)
	file.store_string(to_json(id_to_name))
	file.close()
	
	print("script finished")
