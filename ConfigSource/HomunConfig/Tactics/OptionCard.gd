extends PanelContainer


export var title := ""
export var options:PoolStringArray = ["First Option"]
var clear_button := false
export var card_mode := false
export var advanced := false

func _ready():
	$VBoxContainer/title/title.text = title
	for option in options:
		$VBoxContainer/OptionButton.add_item(option)
		
	clear_button = card_mode
	$VBoxContainer/title/clear.visible = clear_button
	$VBoxContainer/title/separator_mid.visible = clear_button
	
	if advanced and !card_mode:
		add_to_group("advanced")
